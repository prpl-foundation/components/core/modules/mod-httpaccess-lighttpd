# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.5.1 - 2024-11-07(15:35:52 +0000)

### Other

- - make lighttpd restart on failling

## Release v0.5.0 - 2024-11-05(14:27:57 +0000)

### New

- Ensure port changes are applied to Lighty configuration

## Release v0.4.2 - 2024-10-04(11:54:11 +0000)

## Release v0.4.1 - 2024-07-16(08:03:10 +0000)

### Other

- -[plugins] If a plugin requires 'Users.' from tr181-usermanagement, it must be explicitly specified

## Release v0.4.0 - 2024-06-13(11:32:33 +0000)

### Other

- - [tr181-httpaccess] Do not implement Certificate/CACertificate in UserInterface.HTTPAccess.{i}

## Release v0.3.10 - 2024-06-13(07:47:33 +0000)

### Other

- tr181-httpaccess: Support of HTTPS access

## Release v0.3.9 - 2024-06-12(15:08:49 +0000)

### Other

- - Lighttpd profile check fails taking too much time

## Release v0.3.8 - 2024-05-28(13:23:55 +0000)

### Fixes

- - Add support for LLA addresses and expose both LLA and GUA addresses simultaneously

## Release v0.3.7 - 2024-04-02(09:47:46 +0000)

### Fixes

- - Handle properly lighttpd restarts when wan is lost with multiple accesses

## Release v0.3.6 - 2024-03-27(10:24:49 +0000)

### Fixes

- - Filter netmodel addresses to prevent unnecessary lighttpd restarts

## Release v0.3.5 - 2024-02-09(17:26:00 +0000)

### Fixes

- - Stop gracefully lighttpd using SIGINT signal

## Release v0.3.4 - 2024-02-08(14:40:43 +0000)

### Fixes

- - Correct lighttpd configuration file generation as @lla IPv6 addresses have been disabled

## Release v0.3.3 - 2024-02-02(14:05:38 +0000)

### Other

- [CI]lighttpd sometimes does not start

## Release v0.3.2 - 2024-01-29(10:46:59 +0000)

### Fixes

- - Only add interface to ipv6 server address if config type is lla

## Release v0.3.1 - 2024-01-28(08:19:30 +0000)

### Fixes

- - Be more permissive when configuring ipv6 gua server address

## Release v0.3.0 - 2024-01-18(13:32:51 +0000)

### New

- - Add IPv6 support when generating lighttpd config

## Release v0.2.0 - 2023-12-07(10:41:49 +0000)

### New

- - Allow inclusion of additional lighttpd config under dynamically generated sections

## Release v0.1.8 - 2023-11-28(09:29:34 +0000)

### Other

- - Fix typo in vhosts prefixes part

## Release v0.1.7 - 2023-09-04(13:00:51 +0000)

### Changes

- Use lighttpd user and group by default

## Release v0.1.6 - 2023-05-10(07:56:25 +0000)

### Changes

- - [HTTPManager][Login][TR181-HTTPACCESS] Create a session

## Release v0.1.5 - 2022-12-15(10:24:41 +0000)

### Other

- - [reopen][mod-httpaccess-lighttpd] Compilation for wnc fails for v0.1.3

## Release v0.1.4 - 2022-12-15(08:40:17 +0000)

### Other

- - [mod-httpaccess-lighttpd] Compilation for wnc fails for v0.1.3

## Release v0.1.3 - 2022-12-13(09:10:04 +0000)

### Changes

- Refactor HTTPVHost and HTTPProxy to be HTTPAccess

## Release v0.1.3 - 2022-12-13(08:26:46 +0000)

## Release v0.1.2 - 2022-11-21(12:01:09 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-11-16(08:42:38 +0000)

### Other

- Correct install path for lighttpd template

## Release v0.1.0 - 2022-11-02(08:30:26 +0000)

### New

- - [prpl][httpaccess] Create the HTTPAccess first mod for lighttpd

