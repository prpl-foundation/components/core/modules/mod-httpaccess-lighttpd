include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/tr181-httpaccess/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 config/lighttpd.template $(DEST)/etc/lighttpd/lighttpd.template
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(DEST)/webui/etc/lighttpd/conf.d/20-auth.conf
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(DEST)/webui/etc/lighttpd/conf.d/20-authn_file.conf
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(DEST)/webui/etc/lighttpd/conf.d/30-fastcgi.conf
	$(INSTALL) -D -p -m 0644 odl/extensions/$(COMPONENT)_requires.odl $(DEST)/etc/amx/tr181-httpaccess/extensions/$(COMPONENT)_requires.odl

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/tr181-httpaccess/modules/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 config/lighttpd.template $(PKGDIR)/etc/lighttpd/lighttpd.template
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(PKGDIR)/webui/etc/lighttpd/conf.d/20-auth.conf
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(PKGDIR)/webui/etc/lighttpd/conf.d/20-authn_file.conf
	$(INSTALL) -D -p -m 0644 config/20-auth.conf $(PKGDIR)/webui/etc/lighttpd/conf.d/30-fastcgi.conf
	$(INSTALL) -D -p -m 0644 odl/extensions/$(COMPONENT)_requires.odl $(PKGDIR)/etc/amx/tr181-httpaccess/extensions/$(COMPONENT)_requires.odl
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/extensions/$(COMPONENT)_requires.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test