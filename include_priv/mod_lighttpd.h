/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_LIGHTTPD_H__)
#define __MOD_LIGHTTPD_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#define MOD_HTTPACCESS_CTRL "httpaccess-ctrl"
#define MOD_HTTPACCESS_CORE "httpaccess-core"
#define MOD_LIGHTTPD_DIR "/etc/lighttpd/mod/"
#define MOD_LIGHTTPD_CONF_FILE MOD_LIGHTTPD_DIR "mod-lighttpd.conf"
#define MOD_LIGHTTPD_TEMPLATE_FILE MOD_LIGHTTPD_DIR "mod-lighttpd.template"
#define MOD_LIGHTTPD_ACCESS_CONF_DIR MOD_LIGHTTPD_DIR "mod-conf.d/"
#define MOD_LIGHTTPD_CHOWN_OWNER "http:www-data"

#define STRING_EMPTY(TEXT) ((TEXT == NULL) || (*TEXT == 0))

#define ME "mod-lighttpd"

typedef enum access_type_e {
    ACCESS_TYPE_DEFAULT = 0,
    ACCESS_TYPE_PROXY,
    ACCESS_TYPE_VHOST,
    ACCESS_TYPE_LAST
} access_type_t;

int mod_lighttpd_update_accesses(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int mod_lighttpd_delete_access(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int mod_lighttpd_start_previous_access(UNUSED const char* function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret);
int mod_lighttpd_init(const char* function_name, amxc_var_t* args, amxc_var_t* ret);
int mod_lighttpd_check_args(amxc_var_t* access);
int mod_lighttpd_update_access(amxc_var_t* access);
int mod_lighttpd_config_generate_from_accesses_list(amxc_var_t* list);
int mod_lighttpd_config_generate_access_config_ipv4(amxc_var_t* access, amxc_var_t* list, amxc_string_t* config);
int mod_lighttpd_config_generate_access_config_header_ipv4(amxc_var_t* access, amxc_string_t* config);
int mod_lighttpd_config_generate_access_config_ipv6(amxc_var_t* access, size_t ipv6_address_index, amxc_var_t* list, amxc_string_t* config);
int mod_lighttpd_config_generate_access_config_header_ipv6(amxc_var_t* access, size_t ipv6_address_index, amxc_string_t* config);
int mod_lighttpd_config_generate_access_config_footer(amxc_var_t* list, amxc_string_t* config);
int mod_lighttpd_config_get_file_name(amxc_var_t* list, amxc_string_t* out_name);
int mod_lighttpd_config_write(amxc_string_t* config, amxc_string_t* filename);
int mod_lighttpd_config_delete_list(amxc_var_t* list);
int mod_lighttpd_command_check_conf(void);
int mod_lighttpd_command_start_server(void);
int mod_lighttpd_command_stop_server(void);
int mod_lighttpd_command_restart_server(void);
amxc_var_t* mod_lighttpd_get_runned_accesses(void);
amxc_var_t* mod_lighttpd_get_mod_conf(void);
amxc_var_t* mod_lighttpd_get_runned_configs(void);
access_type_t mod_lighttpd_get_access_type(amxc_var_t* access);
int mod_lighttpd_config_generate_global(void);
void mod_lighttpd_create_lists_from_accesses(amxc_var_t* configs_list);
int mod_lighttpd_generate_configs(amxc_var_t* ret);
void mod_lighttpd_clean_mod_conf_dir(void);
void mod_lighttpd_log_runned_access_list(const char* title, amxc_var_t* accesses);

#ifdef __cplusplus
}
#endif

#endif // __MOD_LIGHTTPD_H__
