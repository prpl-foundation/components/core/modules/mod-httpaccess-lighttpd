/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_subproc.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_lighttpd.h"

#ifndef FORCEFUL_TIMEOUT_SHUTDOWN
#define FORCEFUL_TIMEOUT_SHUTDOWN 10
#endif

static amxp_subproc_t* _lighttpd_proc = NULL;

static void mod_lighttpd_server_stopped(UNUSED const char* const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* priv) {
    amxc_var_t ret;
    amxc_var_t send;
    uint32_t exit_code = GET_UINT32(data, "ExitCode");
    uint32_t sig_code = GET_UINT32(data, "Signal");

    amxc_var_init(&ret);
    amxc_var_init(&send);

    if(((exit_code != 0) || (sig_code != 0))) {
        SAH_TRACEZ_ERROR(ME, "lighttpd server (pid=%ul) stop with exit code %d and signal %d, asking for restart", _lighttpd_proc != NULL ? _lighttpd_proc->pid:0, exit_code, sig_code);
        when_failed_trace(amxm_execute_function("self", MOD_HTTPACCESS_CORE, "server-stopped-unexpectedly", &send, &ret), exit, ERROR, "Cannot send restart command to plugin");
    }
exit:
    amxp_slot_disconnect(amxp_subproc_get_sigmngr(_lighttpd_proc), "stop", mod_lighttpd_server_stopped);
    amxp_subproc_delete(&_lighttpd_proc);
    amxc_var_clean(&ret);
    amxc_var_clean(&send);
    return;
}

int mod_lighttpd_command_check_conf(void) {
    int retval = -1;
    int fd_err = -1;
    char buf[511 + 1] = {0};
    amxp_subproc_t* proc = NULL;

    when_failed(amxp_subproc_new(&proc), exit);
    fd_err = amxp_subproc_open_fd(proc, STDERR_FILENO);
    when_failed(amxp_subproc_start_wait(proc, 5000, (char*) "lighttpd", "-tt", "-f", GET_CHAR(mod_lighttpd_get_mod_conf(), "conf-file"), "-D", NULL), exit);
    retval = (int8_t) amxp_subproc_get_exitstatus(proc);
    if(retval) {
        if(read(fd_err, buf, 511) <= 0) {
            strcpy(buf, "No reason available\n");
        }
        SAH_TRACEZ_WARNING(ME, "lighttpd configuration check: %s", buf);
    }
exit:
    amxp_subproc_delete(&proc);
    return retval;
}

int mod_lighttpd_command_start_server(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Start server");
    when_not_null(_lighttpd_proc, exit);
    when_failed(amxp_subproc_new(&_lighttpd_proc), exit);
    when_failed(amxp_slot_connect(amxp_subproc_get_sigmngr(_lighttpd_proc), "stop", NULL, mod_lighttpd_server_stopped, NULL), exit);
    SAH_TRACEZ_INFO(ME, "Starting lighttpd server");
    when_failed(amxp_subproc_start(_lighttpd_proc, (char*) "lighttpd", "-f", GET_CHAR(mod_lighttpd_get_mod_conf(), "conf-file"), "-D", NULL), exit);
    SAH_TRACEZ_INFO(ME, "Lighttpd server started (pid=%ul)", _lighttpd_proc->pid);
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_command_stop_server(void) {
    int retval = -1;
    int ret = -1;

    SAH_TRACEZ_INFO(ME, "Stop server (pid=%ul)", _lighttpd_proc != NULL ? _lighttpd_proc->pid:0);

    if(_lighttpd_proc == NULL) {
        SAH_TRACEZ_WARNING(ME, "lighttpd server not running");
        retval = 0;
        goto exit;
    }

    when_failed(amxp_slot_disconnect(amxp_subproc_get_sigmngr(_lighttpd_proc), "stop", mod_lighttpd_server_stopped), exit);

    if(amxp_subproc_is_running(_lighttpd_proc) == false) {
        SAH_TRACEZ_WARNING(ME, "lighttpd server not running, child process exec must have failed");
        retval = 0;
        when_failed_trace(amxp_subproc_delete(&_lighttpd_proc), exit, WARNING, "failed to delete lighttpd");
        goto exit;
    }

    errno = 0;
    ret = amxp_subproc_kill(_lighttpd_proc, SIGINT);
    if(ret != 0) {
        SAH_TRACEZ_WARNING(ME, "Stopping lighttpd gracefully failed (kill status = %d (%s))", ret, ret != 0 ? strerror(errno):"null");
    }

    if((ret != 0) || (amxp_subproc_wait(_lighttpd_proc, FORCEFUL_TIMEOUT_SHUTDOWN * 1000) != 0)) {
        amxp_subproc_kill(_lighttpd_proc, SIGKILL);
    }

    SAH_TRACEZ_INFO(ME, "Lighttpd server stopped");
    when_failed(amxp_subproc_delete(&_lighttpd_proc), exit);
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_command_restart_server(void) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Restart server");
    when_failed(mod_lighttpd_command_stop_server(), exit);
    when_failed(mod_lighttpd_command_start_server(), exit);
    retval = 0;
exit:
    return retval;
}