/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_lighttpd.h"

int mod_lighttpd_check_args(amxc_var_t* access) {
    int retval = -1;
    amxc_var_t* IPv6_list = NULL;

    when_null(access, exit);
    when_str_empty_trace(GET_CHAR(access, "Alias"), exit, ERROR, "Alias for access is empty");
    when_false(GET_UINT32(access, "Port") != 0, exit);
    when_false_trace((GET_ARG(access, "IPv4") || GET_ARG(access, "IPv6_list")), exit,
                     ERROR, "No IP has been provided for access %s", GET_CHAR(access, "Alias"));
    if(GET_ARG(access, "IPv4") != NULL) {
        when_str_empty_trace(GETP_CHAR(access, "IPv4.Address"), exit,
                             ERROR, "IPv4 address is missing for access %s", GET_CHAR(access, "Alias"));
    }

    IPv6_list = GET_ARG(access, "IPv6_list");
    if(IPv6_list != NULL) {
        bool isValidIPAddressAvailable = false;
        amxc_var_for_each(address, IPv6_list) {
            if(!STRING_EMPTY(GET_CHAR(address, "Address"))) {
                isValidIPAddressAvailable = true;
            }
        }
        when_false_trace(isValidIPAddressAvailable, exit, ERROR, "IPv6 address is missing for access %s", GET_CHAR(access, "Alias"));
    }

    retval = 0;
exit:
    return retval;
}

static amxc_string_t* mod_lighttpd_get_conf_socket_str(amxc_var_t* conf) {
    amxc_string_t* str = NULL;

    amxc_string_new(&str, 0);
    if(GET_ARG(conf, "IPv4") != NULL) {
        amxc_string_setf(str, "%s:%d", GETP_CHAR(conf, "IPv4.Address"), GETP_UINT32(conf, "Port"));
    } else {
        amxc_string_setf(str, "[%s]:%d", GETP_CHAR(conf, "IPv6.Address"), GETP_UINT32(conf, "Port"));
    }
    return str;
}

static amxc_var_t* mod_lighttpd_get_socket_list(amxc_var_t* configs_lists, amxc_string_t* sock_str) {
    amxc_var_t* list = NULL;
    amxc_var_t* runned_accesses = mod_lighttpd_get_runned_accesses();

    amxc_var_for_each(configs_list, configs_lists) {
        amxc_var_t* access = GETP_ARG(runned_accesses, GET_CHAR(configs_list, "0"));
        amxc_string_t* conf_sock_str = mod_lighttpd_get_conf_socket_str(access);

        if(strcmp(amxc_string_get(sock_str, 0), amxc_string_get(conf_sock_str, 0)) == 0) {
            list = configs_list;
        }
        amxc_string_delete(&conf_sock_str);
    }
    return list;
}

static void mod_lighttpd_add_conf_in_ordered_list(amxc_var_t* conf, amxc_var_t* list) {
    uint32_t order = GET_UINT32(conf, "Order");
    amxc_var_t* new_conf_name;

    amxc_var_new(&new_conf_name);
    amxc_var_set_cstring_t(new_conf_name, GET_CHAR(conf, "Alias"));
    amxc_llist_for_each(it, amxc_var_constcast(amxc_llist_t, list)) {
        amxc_var_t* conf_name = amxc_container_of(it, amxc_var_t, lit);
        amxc_var_t* c = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(conf_name, NULL));
        if(order < GET_UINT32(c, "Order")) {
            amxc_llist_it_insert_before(it, &new_conf_name->lit);
            return;
        }
    }
    amxc_var_add(cstring_t, list, amxc_var_constcast(cstring_t, new_conf_name));
    amxc_var_delete(&new_conf_name);
}

void mod_lighttpd_create_lists_from_accesses(amxc_var_t* accesses_lists) {
    amxc_var_t* runned_accesses = mod_lighttpd_get_runned_accesses();

    amxc_var_for_each(conf, runned_accesses) {
        amxc_string_t* sock_str = mod_lighttpd_get_conf_socket_str(conf);
        amxc_var_t* socket_list = mod_lighttpd_get_socket_list(accesses_lists, sock_str);
        if(socket_list == NULL) {
            amxc_var_new(&socket_list);
            amxc_var_set_type(socket_list, AMXC_VAR_ID_LIST);
            amxc_var_add(cstring_t, socket_list, GET_CHAR(conf, "Alias"));
            amxc_var_add(amxc_llist_t, accesses_lists, amxc_var_constcast(amxc_llist_t, socket_list));
            amxc_var_delete(&socket_list);
        } else {
            mod_lighttpd_add_conf_in_ordered_list(conf, socket_list);
        }
        amxc_string_delete(&sock_str);
    }
}

static bool mod_lighttpd_process_port(const amxc_var_t* access, const amxc_var_t* current_running_access) {
    bool update_needed = false;
    uint32_t port;
    uint32_t port_current;

    when_null_trace(access, exit, ERROR, "NULL argument");
    when_null_trace(current_running_access, exit, ERROR, "NULL argument");
    port = GET_UINT32(access, "Port");
    port_current = GET_UINT32(current_running_access, "Port");
    SAH_TRACEZ_INFO(ME, "  - Processing Port:");

    if(port_current == port) {
        SAH_TRACEZ_INFO(ME, "    port is the same, current current(%d)/new(%d) -> no update needed",
                        port_current, port);
    } else {
        SAH_TRACEZ_INFO(ME, "    port has changed, current(%d)/new(%d) -> update needed",
                        port_current, port);
        update_needed = true;
    }
exit:
    return update_needed;
}

static bool mod_lighttpd_process_ipv4(const amxc_var_t* access, const amxc_var_t* current_running_access) {
    bool update_needed = false;
    const char* ipv4_address = GETP_CHAR(access, "IPv4.Address");
    const char* ipv4_address_current = GETP_CHAR(current_running_access, "IPv4.Address");

    SAH_TRACEZ_INFO(ME, "  - Processing IPv4:");

    if(STRING_EMPTY(ipv4_address)) {
        // this section can be removed, if we manage correctly when ip goes down
        SAH_TRACEZ_INFO(ME, "    Address missing, current(%s)/new(%s) -> no update needed",
                        ipv4_address_current, ipv4_address);
    } else if(!STRING_EMPTY(ipv4_address_current) && (strcmp(ipv4_address_current, ipv4_address) == 0)) {
        SAH_TRACEZ_INFO(ME, "    Address is the same, current current(%s)/new(%s) -> no update needed",
                        ipv4_address_current, ipv4_address);
    } else {
        SAH_TRACEZ_INFO(ME, "    Address no more valid, current(%s)/new(%s) -> update needed",
                        ipv4_address_current, ipv4_address);
        update_needed = true;
    }

    return update_needed;
}

static bool mod_lighttpd_process_ipv6(const amxc_var_t* access, const amxc_var_t* current_running_access) {
    bool update_needed = false;
    amxc_var_t* IPv6_list = GET_ARG(access, "IPv6_list");
    amxc_var_t* IPv6_list_current = GET_ARG(current_running_access, "IPv6_list");

    SAH_TRACEZ_INFO(ME, "  - Processing IPv6:");

    amxc_var_for_each(ipv6, IPv6_list) {
        const char* ipv6_address = GET_CHAR(ipv6, "Address");
        const char* ipv6_type_flags = GET_CHAR(ipv6, "TypeFlags");

        if(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, IPv6_list_current))) {
            SAH_TRACEZ_INFO(ME, "    No address running, use (%s) -> update needed", ipv6_address);
            update_needed = true;
            continue; // Process all entries for visibility (break can be added here)
        }

        if(STRING_EMPTY(ipv6_address) || STRING_EMPTY(ipv6_type_flags)) {
            // this section can be removed, if we manage correctly when ip goes down
            SAH_TRACEZ_INFO(ME, "      IPv6 changes but address or typeflags missing, new(%s) -> no update needed", ipv6_address);
            continue;
        }

        amxc_var_for_each(ipv6_current, IPv6_list_current) {
            const char* ipv6_address_current = GET_CHAR(ipv6_current, "Address");
            const char* ipv6_type_flags_current = GET_CHAR(ipv6_current, "TypeFlags");

            if(STRING_EMPTY(ipv6_address_current) || STRING_EMPTY(ipv6_type_flags_current)) {
                SAH_TRACEZ_INFO(ME, "    Current addresses or typeflags are empty -> update needed");
                update_needed = true;
                continue;
            }

            if(!strstr(ipv6_type_flags, ipv6_type_flags_current)) {
                SAH_TRACEZ_INFO(ME, "      Address is not of same type flag, current(%s)/new(%s) -> skip it",
                                ipv6_address_current, ipv6_address);
                continue;
            }

            if(strcmp(ipv6_address_current, ipv6_address) == 0) {
                SAH_TRACEZ_INFO(ME, "      Address is the same, current(%s)/new(%s) -> no update needed",
                                ipv6_address_current, ipv6_address);
                continue;
            }

            SAH_TRACEZ_INFO(ME, "    Address differs, current(%s)/new(%s) -> update needed",
                            ipv6_address_current, ipv6_address);

            update_needed = true; // Process all entries for visibility (break can be added here)
        }
    }

    return update_needed;
}

static void mod_lighttpd_filter_modified_accesses(const amxc_var_t* netmodel_access_list, const amxc_var_t* const runned_accesses) {
    amxc_var_t* current_running_access = NULL;

    amxc_var_for_each(access, netmodel_access_list) {
        bool update_needed = false;

        SAH_TRACEZ_INFO(ME, "--- Filter Access %s", GET_CHAR(access, "Alias"));

        current_running_access = amxc_var_get_key(runned_accesses, GET_CHAR(access, "Alias"), AMXC_VAR_FLAG_DEFAULT);

        // Process IPv4
        update_needed |= mod_lighttpd_process_ipv4(access, current_running_access);

        // Process IPv6
        update_needed |= mod_lighttpd_process_ipv6(access, current_running_access);

        // Process Port
        update_needed |= mod_lighttpd_process_port(access, current_running_access);

        if(update_needed == false) {
            SAH_TRACEZ_INFO(ME, "  -> Remove access %s from processing list", GET_CHAR(access, "Alias"));
            amxc_var_take_it(access);
            amxc_var_delete(&access);
        }
    }
}

static void mod_lighttpd_log_access_list(UNUSED const char* title, amxc_var_t* accesses) {

    if(TRACE_LEVEL_INFO <= sahTraceZoneLevel(sahTraceGetZone(ME))) {
        SAH_TRACEZ_INFO(ME, "%s", title);
        amxc_var_for_each(access, accesses) {

            amxc_var_t* ipv4 = GET_ARG(access, "IPv4");
            amxc_var_t* ipv6_list = GET_ARG(access, "IPv6_list");

            (void) ipv4;
            (void) ipv6_list;

            SAH_TRACEZ_INFO(ME, "  Access %s", GET_CHAR(access, "Alias"));
            SAH_TRACEZ_INFO(ME, "      %s [%s/%s/%s]", GET_CHAR(ipv4, "Address"),
                            GET_CHAR(ipv4, "NetDevName"),
                            GET_CHAR(ipv4, "TypeFlags"),
                            GET_CHAR(ipv4, "Flags"));

            amxc_var_for_each(ipv6, ipv6_list) {
                SAH_TRACEZ_INFO(ME, "      %s [%s/%s/%s]", GET_CHAR(ipv6, "Address"),
                                GET_CHAR(ipv6, "NetDevName"),
                                GET_CHAR(ipv6, "TypeFlags"),
                                GET_CHAR(ipv6, "Flags"));
            }
        }
    }
}

int mod_lighttpd_update_accesses(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    amxc_llist_t* args_filtered = NULL;
    amxc_var_t* runned_accesses = mod_lighttpd_get_runned_accesses();

    when_null(args, exit);
    when_null(ret, exit);

    mod_lighttpd_log_runned_access_list("### Running accesses:", runned_accesses);
    mod_lighttpd_log_access_list("### Accesses to update:", args);

    mod_lighttpd_filter_modified_accesses(args, runned_accesses);
    args_filtered = (amxc_llist_t*) amxc_var_constcast(amxc_llist_t, args);
    if((amxc_llist_is_empty(args_filtered))) {
        SAH_TRACEZ_INFO(ME, "### Re(start) lighttpd server not needed");
        goto exit;
    } else {
        SAH_TRACEZ_INFO(ME, "### Re(start) lighttpd server needed");
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_for_each(access, args) {
        if(mod_lighttpd_check_args(access)) {
            amxc_var_add_key(bool, ret, GET_CHAR(access, "Alias"), false);
            continue;
        }
        if(GET_ARG(runned_accesses, GET_CHAR(access, "Alias")) == NULL) {
            amxc_var_add_key(amxc_htable_t, runned_accesses, GET_CHAR(access, "Alias"), amxc_var_constcast(amxc_htable_t, access));
        } else {
            amxc_var_t* tmp = amxc_var_take_key(runned_accesses, GET_CHAR(access, "Alias"));
            amxc_var_delete(&tmp);
            amxc_var_add_key(amxc_htable_t, runned_accesses, GET_CHAR(access, "Alias"), amxc_var_constcast(amxc_htable_t, access));
        }
    }
    when_failed(mod_lighttpd_generate_configs(ret), exit);

    mod_lighttpd_command_restart_server();

exit:
    return 0;
}

int mod_lighttpd_start_previous_access(UNUSED const char* function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    return mod_lighttpd_command_start_server();
}
