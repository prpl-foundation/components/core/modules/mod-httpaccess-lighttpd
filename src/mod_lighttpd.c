/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_lighttpd.h"

static amxc_var_t* runned_accesses = NULL;
static amxc_var_t* mod_config = NULL;
static amxc_var_t* runned_configs = NULL;

amxc_var_t* mod_lighttpd_get_runned_accesses(void) {
    return runned_accesses;
}

amxc_var_t* mod_lighttpd_get_mod_conf(void) {
    return mod_config;
}

amxc_var_t* mod_lighttpd_get_runned_configs(void) {
    return runned_configs;
}

static void mod_lighttpd_set_list_status(amxc_var_t* list, amxc_var_t* ret, bool status) {
    amxc_var_for_each(access, list) {
        amxc_var_add_key(bool, ret, GET_CHAR(access, NULL), status);
    }
}

access_type_t mod_lighttpd_get_access_type(amxc_var_t* access) {
    struct {const char* mnemo; access_type_t value;} access_type_array[] = {
        {"Default", ACCESS_TYPE_DEFAULT},
        {"Proxy", ACCESS_TYPE_PROXY},
        {"VHost", ACCESS_TYPE_VHOST}
    };
    access_type_t type = ACCESS_TYPE_LAST;
    when_null(access, exit);
    when_null(GETP_ARG(access, "Type"), exit);

    for(uint32_t i = 0; i < ACCESS_TYPE_LAST; ++i) {
        if(strcmp(GET_CHAR(access, "Type"), access_type_array[i].mnemo) == 0) {
            type = access_type_array[i].value;
            break;
        }
    }
exit:
    return type;
}

int mod_lighttpd_generate_configs(amxc_var_t* ret) {
    int retval = -1;
    amxc_var_t accesses_lists;

    mod_lighttpd_log_runned_access_list("### Config generated based on:", mod_lighttpd_get_runned_accesses());

    when_failed(amxc_var_init(&accesses_lists), exit);
    amxc_var_set_type(&accesses_lists, AMXC_VAR_ID_LIST);
    mod_lighttpd_clean_mod_conf_dir();
    when_true_trace(mod_lighttpd_config_generate_global() != 0, exit, ERROR, "Error while generating global configuration file");
    mod_lighttpd_create_lists_from_accesses(&accesses_lists);

    amxc_llist_for_each(list_it, amxc_var_constcast(amxc_llist_t, &accesses_lists)) {
        amxc_var_t* list = amxc_container_of(list_it, amxc_var_t, lit);
        mod_lighttpd_config_generate_from_accesses_list(list);

        if(mod_lighttpd_command_check_conf() != 0) {
            SAH_TRACEZ_ERROR(ME, "Error with the new configuration file for the socket of %s. Deleting it.", GETP_CHAR(list, "0"));
            mod_lighttpd_config_delete_list(list);
            mod_lighttpd_set_list_status(list, ret, false);
            continue;
        }
        mod_lighttpd_set_list_status(list, ret, true);
    }

    retval = 0;
exit:
    amxc_var_clean(&accesses_lists);
    return retval;
}

static AMXM_CONSTRUCTOR mod_lighttpd_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxc_var_new(&runned_accesses);
    amxc_var_set_type(runned_accesses, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_config);
    amxc_var_new(&runned_configs);
    amxc_var_set_type(runned_configs, AMXC_VAR_ID_LIST);
    amxm_module_register(&mod, so, MOD_HTTPACCESS_CTRL);
    amxm_module_add_function(mod, "init", mod_lighttpd_init);
    amxm_module_add_function(mod, "start", mod_lighttpd_start_previous_access);
    amxm_module_add_function(mod, "update-accesses", mod_lighttpd_update_accesses);
    amxm_module_add_function(mod, "delete-access", mod_lighttpd_delete_access);
    return 0;
}

static AMXM_DESTRUCTOR mod_lighttpd_stop(void) {
    amxc_var_delete(&runned_accesses);
    amxc_var_delete(&mod_config);
    amxc_var_delete(&runned_configs);
    return mod_lighttpd_command_stop_server();
}
