/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_lighttpd.h"

static int mod_lighttpd_generate_access_config_alias(amxc_var_t* access, amxc_string_t* config) {
    int retval = -1;
    const char* alias = GET_CHAR(access, "Alias");

    when_failed(amxc_string_appendf(config, "  setenv.add-request-header = (\"HTTPAccessPath\" => \"UserInterface.HTTPAccess.%s.\")\n", alias), exit);
    retval = 0;
exit:
    return retval;
}

static int mod_lighttpd_generate_access_config_extra_conf(amxc_var_t* access, amxc_string_t* config) {
    int retval = -1;

    when_null(access, exit);
    when_null(config, exit);
    when_str_empty_status(GET_CHAR(access, "extra-conf-file"), exit, retval = 0);
    amxc_string_appendf(config, "%sinclude \"%s\"\n", (mod_lighttpd_get_access_type(access) == ACCESS_TYPE_DEFAULT) ? "  " : "    ",
                        GET_CHAR(access, "extra-conf-file"));
    retval = 0;
exit:
    return retval;
}

static void mod_lighttpd_generate_access_config_proxy(amxc_var_t* access, amxc_string_t* config) {
    amxc_var_t csv_list;

    if(GET_CHAR(access, "Hosts") && (strlen(GET_CHAR(access, "Hosts")) > 0)) {
        amxc_var_init(&csv_list);
        amxc_var_convert(&csv_list, GET_ARG(access, "Hosts"), AMXC_VAR_ID_LIST);
        amxc_var_for_each(host, &csv_list) {
            amxc_string_appendf(config, "  $HTTP[\"host\"] =~ \"(^|\\.)%s$\" {\n", GET_CHAR(host, NULL));
            mod_lighttpd_generate_access_config_extra_conf(access, config);
            amxc_string_appendf(config, "    proxy.server = (\"\" => ((\"host\" => \"%s\", \"port\" => \"%d\" )))\n",
                                GET_CHAR(access, "ProxyIP"), GET_UINT32(access, "ProxyPort"));
            amxc_string_appendf(config, "  }\n");
        }
        amxc_var_clean(&csv_list);
    }
    if(GET_CHAR(access, "Prefixes") && (strlen(GET_CHAR(access, "Prefixes")) > 0)) {
        amxc_var_init(&csv_list);
        amxc_var_convert(&csv_list, GET_ARG(access, "Prefixes"), AMXC_VAR_ID_LIST);
        amxc_var_for_each(prefix, &csv_list) {
            amxc_string_appendf(config, "  $HTTP[\"url\"] =~ \"^/%s(/.*)?$\" {\n", GET_CHAR(prefix, NULL));
            mod_lighttpd_generate_access_config_extra_conf(access, config);
            amxc_string_appendf(config, "    proxy.server = (\"\" => ((\"host\" => \"%s\", \"port\" => \"%d\" )))\n",
                                GET_CHAR(access, "ProxyIP"), GET_UINT32(access, "ProxyPort"));
            amxc_string_appendf(config, "    proxy.header = (\"map-urlpath\" => (\"/%s\" => \"/\"))\n",
                                GET_CHAR(prefix, NULL));
            amxc_string_appendf(config, "  }\n");
        }
        amxc_var_clean(&csv_list);
    }
}

static void mod_lighttpd_generate_access_config_vhost(amxc_var_t* access, amxc_string_t* config) {
    amxc_var_t csv_list;

    if(GET_CHAR(access, "Hosts") && (strlen(GET_CHAR(access, "Hosts")) > 0)) {
        amxc_var_init(&csv_list);
        amxc_var_convert(&csv_list, GET_ARG(access, "Hosts"), AMXC_VAR_ID_LIST);
        amxc_var_for_each(host, &csv_list) {
            amxc_string_appendf(config, "  $HTTP[\"host\"] =~ \"(^|\\.)%s$\" {\n", GET_CHAR(host, NULL));
            mod_lighttpd_generate_access_config_extra_conf(access, config);
            amxc_string_appendf(config, "    server.document-root = \"%s\"\n", GET_CHAR(access, "VHostDocumentRoot"));
            amxc_string_appendf(config, "  }\n");
        }
        amxc_var_clean(&csv_list);
    }
    if(GET_CHAR(access, "Prefixes") && (strlen(GET_CHAR(access, "Prefixes")) > 0)) {
        amxc_var_init(&csv_list);
        amxc_var_convert(&csv_list, GET_ARG(access, "Prefixes"), AMXC_VAR_ID_LIST);
        amxc_var_for_each(prefix, &csv_list) {
            amxc_string_appendf(config, "  $HTTP[\"url\"] =~ \"^/%s(/.*)?$\" {\n", GET_CHAR(prefix, NULL));
            mod_lighttpd_generate_access_config_extra_conf(access, config);
            amxc_string_appendf(config, "    server.document-root = \"%s\"\n", GET_CHAR(access, "VHostDocumentRoot"));
            amxc_string_appendf(config, "  }\n");
        }
        amxc_var_clean(&csv_list);
    }
}

int mod_lighttpd_config_get_file_name(amxc_var_t* list, amxc_string_t* out_name) {
    amxc_var_t* first = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(amxc_var_get_first(list), NULL));
    amxc_var_t* last = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(amxc_var_get_last(list), NULL));

    amxc_string_setf(out_name, "%s/%02d-%s.conf", GET_CHAR(mod_lighttpd_get_mod_conf(), "mod-dir"), GET_UINT32(first, "Order"), GET_CHAR(last, "Alias"));
    return 0;
}

static int mod_lighttpd_config_generate_access(amxc_var_t* access, amxc_string_t* config) {
    int retval = -1;
    access_type_t access_type = ACCESS_TYPE_LAST;

    when_null(access, exit);
    when_null(config, exit);
    access_type = mod_lighttpd_get_access_type(access);
    when_true(access_type == ACCESS_TYPE_LAST, exit);
    switch(access_type) {
    case ACCESS_TYPE_PROXY:
        mod_lighttpd_generate_access_config_proxy(access, config);
        break;
    case ACCESS_TYPE_VHOST:
        mod_lighttpd_generate_access_config_vhost(access, config);
        break;
    case ACCESS_TYPE_DEFAULT:
        mod_lighttpd_generate_access_config_extra_conf(access, config);
        break;
    default:
        break;
    }
    retval = 0;
exit:
    return retval;
}

static int mod_lighttpd_generate_access_config_extra_conf_default(amxc_var_t* access, amxc_string_t* config) {
    int retval = -1;

    when_null(access, exit);
    when_null(config, exit);
    when_str_empty_status(GET_CHAR(access, "extra-conf-default-file"), exit, retval = 0);
    amxc_string_appendf(config, "  include \"%s\"\n", GET_CHAR(access, "extra-conf-default-file"));
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_config_generate_access_config_header_ipv4(amxc_var_t* access, amxc_string_t* config) {
    int retval = -1;
    when_null(access, exit);
    when_null(config, exit);

    when_failed(amxc_string_appendf(config, "$SERVER[\"socket\"] == \"%s:%d\" {\n",
                                    GETP_CHAR(access, "IPv4.Address"),
                                    GET_UINT32(access, "Port")), exit);

    when_failed(mod_lighttpd_generate_access_config_alias(access, config), exit);
    when_failed(mod_lighttpd_generate_access_config_extra_conf_default(access, config), exit);
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_config_generate_access_config_header_ipv6(amxc_var_t* access, size_t ipv6_address_index, amxc_string_t* config) {
    int retval = -1;
    const cstring_t type = NULL;
    amxc_var_t* ipv6_address = NULL;

    when_null(access, exit);
    when_null(config, exit);

    ipv6_address = amxc_var_get_index(GETP_ARG(access, "IPv6_list"), ipv6_address_index, AMXC_VAR_FLAG_DEFAULT);
    when_null(ipv6_address, exit);

    type = GETP_CHAR(ipv6_address, "TypeFlags");
    when_str_empty(type, exit);

    if(strstr(type, "@gua") != NULL) {
        when_failed(amxc_string_appendf(config, "$SERVER[\"socket\"] == \"[%s]:%d\" {\n",
                                        GETP_CHAR(ipv6_address, "Address"),
                                        GET_UINT32(access, "Port")), exit);
    } else if(strstr(type, "@lla") != NULL) {
        when_failed(amxc_string_appendf(config, "$SERVER[\"socket\"] == \"[%s%%%s]:%d\" {\n",
                                        GETP_CHAR(ipv6_address, "Address"),
                                        GETP_CHAR(ipv6_address, "NetDevName"),
                                        GET_UINT32(access, "Port")), exit);
    }

    when_failed(mod_lighttpd_generate_access_config_alias(access, config), exit);
    when_failed(mod_lighttpd_generate_access_config_extra_conf_default(access, config), exit);
    retval = 0;

exit:
    return retval;
}

int mod_lighttpd_config_generate_access_config_footer(amxc_var_t* list, amxc_string_t* config) {
    int retval = -1;

    when_null(list, exit);
    when_null(config, exit);

    when_failed(amxc_string_append(config, "}\n# Config generated for", 24), exit);
    amxc_llist_for_each(conf, amxc_var_constcast(amxc_llist_t, list)) {
        when_failed(amxc_string_appendf(config, " %s", GET_CHAR(amxc_container_of(conf, amxc_var_t, lit), NULL)), exit);
    }
    when_failed(amxc_string_append(config, "\n", 1), exit);
    retval = 0;
exit:
    return retval;
}


static int mod_lighttpd_config_fetch_template(amxc_string_t* template) {
    int fd = -1;
    int ret = -1;
    int retval = -1;
    char buf[512] = {0};

    fd = open(GET_CHAR(mod_lighttpd_get_mod_conf(), "conf-template"), O_RDONLY);
    when_true(fd == -1, exit);
    while((ret = read(fd, buf, 512)) > 0) {
        amxc_string_append(template, buf, ret);
    }
    when_true(ret == -1, exit);
    retval = 0;
exit:
    close(fd);
    return retval;
}

static int mod_lighttpd_config_generate_global_from_template(amxc_string_t* template) {
    int fd = -1;
    int retval = -1;
    unsigned int ret = 0, template_length;

    fd = open(GET_CHAR(mod_lighttpd_get_mod_conf(), "conf-file"), O_CREAT | O_TRUNC | O_WRONLY, 0644);
    when_true_trace(fd == -1, exit, WARNING, "Couldn't open config file (%s)", GET_CHAR(mod_lighttpd_get_mod_conf(), "conf-file"));
    when_failed(amxc_string_appendf(template, "\ninclude \"%s/*\"\n", GET_CHAR(mod_lighttpd_get_mod_conf(), "mod-dir")), exit);
    template_length = amxc_string_buffer_length(template) - 1;
    while(ret < template_length) {
        retval = write(fd, amxc_string_get(template, 0) + ret, template_length - ret);
        when_true(retval == -1, exit);
        ret += retval;
    }
    retval = 0;
exit:
    close(fd);
    return retval;
}

int mod_lighttpd_config_generate_global(void) {
    int retval = -1;
    amxc_string_t template;

    when_failed(amxc_string_init(&template, 0), exit);
    when_failed(mod_lighttpd_config_fetch_template(&template), exit);
    when_failed(mod_lighttpd_config_generate_global_from_template(&template), exit);
    retval = 0;
exit:
    amxc_string_clean(&template);
    return retval;
}

int mod_lighttpd_config_write(amxc_string_t* config, amxc_string_t* filename) {
    int retval = -1;
    int fd = -1;

    when_null(config, exit);
    when_null(filename, exit);
    fd = open(amxc_string_get(filename, 0), O_WRONLY | O_TRUNC | O_CREAT, 0644);
    when_false_trace(fd != -1, exit, ERROR, "Couldn't open config file %s", amxc_string_get(filename, 0));
    when_true(write(fd, amxc_string_get(config, 0), amxc_string_buffer_length(config) - 1) == -1, exit);
    retval = 0;
exit:
    close(fd);
    return retval;
}

int mod_lighttpd_config_generate_access_config_ipv4(amxc_var_t* access, amxc_var_t* list, amxc_string_t* config) {
    int retval = -1;

    when_failed(mod_lighttpd_config_generate_access_config_header_ipv4(access, config), exit);

    amxc_llist_for_each(access_name, amxc_var_constcast(amxc_llist_t, list)) {
        amxc_var_t* access_to_update = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(amxc_container_of(access_name, amxc_var_t, lit), NULL));
        mod_lighttpd_config_generate_access(access_to_update, config);
    }
    when_failed(mod_lighttpd_config_generate_access_config_footer(list, config), exit);
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_config_generate_access_config_ipv6(amxc_var_t* access, size_t ipv6_address_index, amxc_var_t* list, amxc_string_t* config) {
    int retval = -1;

    when_failed(mod_lighttpd_config_generate_access_config_header_ipv6(access, ipv6_address_index, config), exit);

    amxc_llist_for_each(access_name, amxc_var_constcast(amxc_llist_t, list)) {
        amxc_var_t* access_to_update = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(amxc_container_of(access_name, amxc_var_t, lit), NULL));
        mod_lighttpd_config_generate_access(access_to_update, config);
    }
    when_failed(mod_lighttpd_config_generate_access_config_footer(list, config), exit);
    retval = 0;
exit:
    return retval;
}

int mod_lighttpd_config_generate_from_accesses_list(amxc_var_t* list) {
    int retval = -1;
    amxc_string_t config;
    amxc_string_t filename;
    amxc_var_t* access = NULL;
    amxc_var_t* IPv6_list = NULL;

    amxc_string_init(&config, 0);
    amxc_string_init(&filename, 0);

    when_null(list, exit);
    when_str_empty(GET_CHAR(list, "0"), exit);
    access = GET_ARG(mod_lighttpd_get_runned_accesses(), GET_CHAR(list, "0"));
    when_null(access, exit);

    if(GETP_CHAR(access, "IPv4.Address") != NULL) {
        when_failed(mod_lighttpd_config_generate_access_config_ipv4(access, list, &config), exit);
    }

    IPv6_list = GET_ARG(access, "IPv6_list");
    if(IPv6_list != NULL) {
        size_t ipv6_address_index = 0;
        amxc_var_for_each(ipv6_address, IPv6_list) {
            when_failed(mod_lighttpd_config_generate_access_config_ipv6(access, ipv6_address_index, list, &config), exit);
            ipv6_address_index++;
        }
    }

    when_failed(mod_lighttpd_config_get_file_name(list, &filename), exit);
    when_failed(mod_lighttpd_config_write(&config, &filename), exit);
    retval = 0;

exit:
    amxc_string_clean(&config);
    amxc_string_clean(&filename);
    return retval;
}

int mod_lighttpd_config_delete_list(amxc_var_t* list) {
    int retval = -1;
    amxc_string_t filename;

    amxc_string_init(&filename, 0);
    mod_lighttpd_config_get_file_name(list, &filename);
    retval = 0;
    when_str_empty(amxc_string_get(&filename, 0), exit);
    retval = unlink(amxc_string_get(&filename, 0));
exit:
    amxc_string_clean(&filename);
    return retval;
}
