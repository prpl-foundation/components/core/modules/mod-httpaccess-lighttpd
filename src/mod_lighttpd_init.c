/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <fcntl.h>
#include <unistd.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp_expression.h>
#include <amxp/amxp.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_lighttpd.h"

static inline int char_is_number(char c) {
    return (c >= '0' && c <= '9');
}

void mod_lighttpd_clean_mod_conf_dir(void) {
    DIR* conf_dir = NULL;
    amxc_string_t filepath;
    const char* conf_dir_name = GET_CHAR(mod_lighttpd_get_mod_conf(), "mod-dir");

    conf_dir = opendir(conf_dir_name);
    when_null(conf_dir, exit);
    for(struct dirent* file = readdir(conf_dir); file; file = readdir(conf_dir)) {
        if(!((strlen(file->d_name) > 3) &&
             char_is_number(file->d_name[0]) && char_is_number(file->d_name[1]) &&
             (file->d_name[2] == '-'))) {
            continue;
        }
        amxc_string_init(&filepath, 0);
        amxc_string_setf(&filepath, "%s%s", conf_dir_name, file->d_name);
        unlink(amxc_string_get(&filepath, 0));
        amxc_string_clean(&filepath);
    }
    closedir(conf_dir);
exit:
    return;
}

static void mod_lighttpd_create_mod_conf_dir(void) {
    const char* conf_dir_name = GET_CHAR(mod_lighttpd_get_mod_conf(), "mod-dir");

    if(access(conf_dir_name, F_OK) == -1) {
        if(mkdir(conf_dir_name, 0777)) {
            SAH_TRACEZ_ERROR(ME, "Couldn't create config directory %s", conf_dir_name);
        }
    }
}

static void mod_lighttpd_change_dirent_owner(const char* dir_name, uid_t uid, gid_t gid) {
    DIR* dir = NULL;
    amxc_string_t filename;
    struct stat fst = {0};

    amxc_string_init(&filename, 0);
    amxc_string_setf(&filename, "%s/", dir_name);
    dir = opendir(dir_name);
    when_null(dir, exit);
    if(chown(amxc_string_get(&filename, 0), uid, gid) == -1) {
        SAH_TRACEZ_WARNING(ME, "Couldn't chown for %s %d:%d", amxc_string_get(&filename, 0), uid, gid);
    }
    for(struct dirent* file = readdir(dir); file; file = readdir(dir)) {
        if((strcmp(file->d_name, ".") == 0) || (strcmp(file->d_name, "..") == 0)) {
            continue;
        }
        amxc_string_setf(&filename, "%s/%s", dir_name, file->d_name);
        stat(amxc_string_get(&filename, 0), &fst);
        if(S_ISDIR(fst.st_mode)) {
            mod_lighttpd_change_dirent_owner(amxc_string_get(&filename, 0), uid, gid);
        } else {
            if(chown(amxc_string_get(&filename, 0), uid, gid) == -1) {
                SAH_TRACEZ_WARNING(ME, "Couldn't chown for %s %d:%d", amxc_string_get(&filename, 0), uid, gid);
            }
        }
    }
exit:
    closedir(dir);
    amxc_string_clean(&filename);
    return;
}

static void mod_lighttpd_chown_directory(void) {
    const char* chown_dir = GET_CHAR(mod_lighttpd_get_mod_conf(), "chown-dir");
    const char* chown_owner = GET_CHAR(mod_lighttpd_get_mod_conf(), "chown-owner");
    char buf[127 + 1] = {0};
    size_t sep_idx = 0;
    const struct passwd* pwd = NULL;
    const struct group* grp = NULL;

    when_str_empty(chown_dir, exit);
    when_null_trace(strchr(chown_owner, ':'), exit, ERROR, "mod-lighttpd.chown-owner is not conform");
    sep_idx = strcspn(chown_owner, ":");
    when_true_trace(sep_idx > 127, exit, ERROR, "mod-ligttpd.chown-owner user is too long");
    strncpy(buf, chown_owner, sep_idx);
    pwd = getpwnam(buf);
    when_null_trace(pwd, exit, ERROR, "mod-ligttpd.chown-owner user couldn't be found");
    grp = getgrnam(chown_owner + sep_idx + 1);
    when_null_trace(pwd, exit, ERROR, "mod-ligttpd.chown-owner group couldn't be found");
    mod_lighttpd_change_dirent_owner(chown_dir, pwd->pw_uid, grp->gr_gid);
exit:
    return;
}

int mod_lighttpd_init(UNUSED const char* function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxc_var_t* mod_conf = mod_lighttpd_get_mod_conf();

    when_null(args, exit);
    when_null(mod_conf, exit);

    if(GET_ARG(args, "mod-lighttpd")) {
        amxc_var_copy(mod_conf, GET_ARG(args, "mod-lighttpd"));
    } else {
        amxc_var_set_type(mod_conf, AMXC_VAR_ID_HTABLE);
    }
    if(GET_ARG(mod_conf, "conf-file") == NULL) {
        SAH_TRACEZ_INFO(ME, "conf-file variable is missing. Using default value (%s)", MOD_LIGHTTPD_CONF_FILE);
        amxc_var_add_key(cstring_t, mod_conf, "conf-file", MOD_LIGHTTPD_CONF_FILE);
    }
    if(GET_ARG(mod_conf, "conf-template") == NULL) {
        SAH_TRACEZ_INFO(ME, "conf-template variable is missing. Using default value (%s)", MOD_LIGHTTPD_TEMPLATE_FILE);
        amxc_var_add_key(cstring_t, mod_conf, "conf-template", MOD_LIGHTTPD_TEMPLATE_FILE);
    }
    if(GET_ARG(mod_conf, "mod-dir") == NULL) {
        SAH_TRACEZ_INFO(ME, "mod-dir variable is missing. Using default value (%s)", MOD_LIGHTTPD_ACCESS_CONF_DIR);
        amxc_var_add_key(cstring_t, mod_conf, "mod-dir", MOD_LIGHTTPD_ACCESS_CONF_DIR);
    }
    if(GET_ARG(mod_conf, "chown-owner") == NULL) {
        SAH_TRACEZ_INFO(ME, "chown-owner variable is missing. Using default value (%s)", MOD_LIGHTTPD_CHOWN_OWNER);
        amxc_var_add_key(cstring_t, mod_conf, "chown-owner", MOD_LIGHTTPD_CHOWN_OWNER);
    }
    mod_lighttpd_clean_mod_conf_dir();
    mod_lighttpd_create_mod_conf_dir();
    mod_lighttpd_chown_directory();
exit:
    return 0;
}