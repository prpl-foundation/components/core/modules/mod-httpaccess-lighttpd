/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>

#include "mod_lighttpd.h"

#include "test_mod_lighttpd_rpc.h"
#include "mock.h"

static void fill_access_ipv4_entry(amxc_var_t* ipv4, const char* address) {
    amxc_var_set_type(ipv4, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ipv4, "Address", address);
    amxc_var_add_key(cstring_t, ipv4, "Family", "ipv4");
    amxc_var_add_key(cstring_t, ipv4, "Flags", "permanent");
    amxc_var_add_key(cstring_t, ipv4, "NetDevName", "br-lan");
    amxc_var_add_key(cstring_t, ipv4, "NetModelIntf", "ip-lan");
    amxc_var_add_key(cstring_t, ipv4, "Peer", "");
    amxc_var_add_key(uint32_t, ipv4, "PrefixLen", 24);
    amxc_var_add_key(cstring_t, ipv4, "Scope", "global");
    amxc_var_add_key(cstring_t, ipv4, "TypeFlags", "@private");
}

static void fill_access_ipv6_entry(amxc_var_t* ipv6, const char* address, const char* typeFlags) {
    amxc_var_set_type(ipv6, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, ipv6, "Address", address);
    amxc_var_add_key(cstring_t, ipv6, "Family", "ipv6");
    amxc_var_add_key(cstring_t, ipv6, "Flags", "permanent");
    amxc_var_add_key(cstring_t, ipv6, "NetDevName", "br-lan");
    amxc_var_add_key(cstring_t, ipv6, "NetModelIntf", "ip-lan");
    amxc_var_add_key(cstring_t, ipv6, "Peer", "");
    amxc_var_add_key(uint32_t, ipv6, "PrefixLen", 24);
    amxc_var_add_key(cstring_t, ipv6, "Scope", "global");
    amxc_var_add_key(cstring_t, ipv6, "TypeFlags", typeFlags);
}

static void fill_access(amxc_var_t* access, const char* access_name, UNUSED amxc_var_t* ipv4_entry, UNUSED amxc_var_t* ipv6_entry) {
    amxc_var_add_key(cstring_t, access, "Alias", access_name);
    amxc_var_add_key(cstring_t, access, "Hosts", "");
    amxc_var_add_key(uint32_t, access, "Order", 1);
    amxc_var_add_key(uint32_t, access, "Port", 80);
    amxc_var_add_key(cstring_t, access, "Prefixes", "");
    amxc_var_add_key(cstring_t, access, "Protocol", "HTTP");
    amxc_var_add_key(cstring_t, access, "Type", "Default");

    if(ipv4_entry != NULL) {
        amxc_var_add_key(amxc_htable_t, access, "IPv4", amxc_var_get_const_amxc_htable_t(ipv4_entry));
    }

    if(ipv6_entry != NULL) {
        amxc_var_add_key(amxc_llist_t, access, "IPv6_list", amxc_var_get_const_amxc_llist_t(ipv6_entry));
    }
}

void test_mod_lighttpd_rpc_update_accesses_and_none_is_running(UNUSED void** state) {
    int result = 0;
    amxc_var_t ret;
    amxc_var_t running_access_list;
    amxc_var_t new_accesses;
    amxc_var_t local_gui_access_expected;

    amxc_var_init(&local_gui_access_expected);
    amxc_var_init(&ret);

    // running access
    amxc_var_init(&running_access_list);
    amxc_var_set_type(&running_access_list, AMXC_VAR_ID_HTABLE);

    // create access given by netmodel
    amxc_var_init(&new_accesses);
    amxc_var_set_type(&new_accesses, AMXC_VAR_ID_LIST);

    amxc_var_t* local_gui_access = amxc_var_add_new(&new_accesses);
    amxc_var_set_type(local_gui_access, AMXC_VAR_ID_HTABLE);

    amxc_var_t* ipv4 = NULL;
    amxc_var_new(&ipv4);
    fill_access_ipv4_entry(ipv4, "192.168.1.1");

    amxc_var_t* ipv6_list = NULL;
    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    amxc_var_t* ipv6_gua = amxc_var_add_new(ipv6_list);
    fill_access_ipv6_entry(ipv6_gua, "2001:470:cb55:39f0::1", "@gua");
    amxc_var_t* ipv6_lla = amxc_var_add_new(ipv6_list);
    fill_access_ipv6_entry(ipv6_lla, "fe80::2e93:fbff:fe15:72a1", "@lla");

    fill_access(local_gui_access, "LocalGUI", ipv4, ipv6_list);

    // result expected
    amxc_var_copy(&local_gui_access_expected, local_gui_access);

    will_return(__wrap_mod_lighttpd_get_runned_accesses, &running_access_list);
    will_return(__wrap_mod_lighttpd_generate_configs, 0);
    will_return(__wrap_mod_lighttpd_command_restart_server, 0);

    // test
    mod_lighttpd_update_accesses("update-accesses", &new_accesses, &ret);

    // assert
    assert_int_equal(amxc_var_compare(&local_gui_access_expected, GET_ARG(&running_access_list, "LocalGUI"), &result), 0);
    assert_int_equal(result, 0);

    amxc_var_delete(&ipv4);
    amxc_var_delete(&ipv6_list);
    amxc_var_clean(&new_accesses);
    amxc_var_clean(&running_access_list);
    amxc_var_clean(&local_gui_access_expected);
    amxc_var_clean(&ret);
}

void test_mod_lighttpd_rpc_update_accesses_with_lla_address_only_and_none_is_running(UNUSED void** state) {
    int result = 0;
    amxc_var_t ret;
    amxc_var_t running_access_list;
    amxc_var_t new_accesses;
    amxc_var_t local_gui_access_expected;

    amxc_var_init(&local_gui_access_expected);
    amxc_var_init(&ret);

    // running access
    amxc_var_init(&running_access_list);
    amxc_var_set_type(&running_access_list, AMXC_VAR_ID_HTABLE);

    // create access given by netmodel
    amxc_var_init(&new_accesses);
    amxc_var_set_type(&new_accesses, AMXC_VAR_ID_LIST);

    amxc_var_t* local_gui_access = local_gui_access = amxc_var_add_new(&new_accesses);
    amxc_var_set_type(local_gui_access, AMXC_VAR_ID_HTABLE);

    amxc_var_t* ipv4 = NULL;
    amxc_var_new(&ipv4);
    fill_access_ipv4_entry(ipv4, "192.168.1.1");

    amxc_var_t* ipv6_list = NULL;
    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    amxc_var_t* ipv6_lla = amxc_var_add_new(ipv6_list);
    fill_access_ipv6_entry(ipv6_lla, "fe80::2e93:fbff:fe15:72a1", "@lla");

    fill_access(local_gui_access, "LocalGUI", ipv4, ipv6_list);

    // result expected, remove ip6 since it non @gua
    amxc_var_copy(&local_gui_access_expected, local_gui_access);

    will_return(__wrap_mod_lighttpd_get_runned_accesses, &running_access_list);
    will_return(__wrap_mod_lighttpd_generate_configs, 0);
    will_return(__wrap_mod_lighttpd_command_restart_server, 0);

    // test
    mod_lighttpd_update_accesses("update-accesses", &new_accesses, &ret);

    // assert
    assert_int_equal(amxc_var_compare(&local_gui_access_expected, GET_ARG(&running_access_list, "LocalGUI"), &result), 0);
    assert_int_equal(result, 0);

    amxc_var_delete(&ipv4);
    amxc_var_delete(&ipv6_list);
    amxc_var_clean(&new_accesses);
    amxc_var_clean(&running_access_list);
    amxc_var_clean(&local_gui_access_expected);
    amxc_var_clean(&ret);
}

void test_mod_lighttpd_rpc_update_accesses_but_same_running(UNUSED void** state) {
    int result = 0;
    amxc_var_t ret;
    amxc_var_t running_access_list;
    amxc_var_t new_accesses;
    amxc_var_t local_gui_access_expected;

    amxc_var_init(&local_gui_access_expected);
    amxc_var_init(&ret);

    // create access given by netmodel
    amxc_var_init(&new_accesses);
    amxc_var_set_type(&new_accesses, AMXC_VAR_ID_LIST);

    amxc_var_t* local_gui_access = local_gui_access = amxc_var_add_new(&new_accesses);
    amxc_var_set_type(local_gui_access, AMXC_VAR_ID_HTABLE);

    amxc_var_t* ipv4 = NULL;
    amxc_var_new(&ipv4);
    fill_access_ipv4_entry(ipv4, "192.168.1.1");

    amxc_var_t* ipv6_list = NULL;
    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    amxc_var_t* ipv6_gua = amxc_var_add_new(ipv6_list);
    fill_access_ipv6_entry(ipv6_gua, "2001:470:cb55:39f0::1", "@gua");

    fill_access(local_gui_access, "LocalGUI", ipv4, ipv6_list);

    // running access
    amxc_var_init(&running_access_list);
    amxc_var_set_type(&running_access_list, AMXC_VAR_ID_HTABLE);
    amxc_var_t* local_gui_access_running = amxc_var_add_new_key(&running_access_list, "LocalGUI");
    amxc_var_copy(local_gui_access_running, local_gui_access);

    // result expected
    amxc_var_copy(&local_gui_access_expected, local_gui_access);
    will_return(__wrap_mod_lighttpd_get_runned_accesses, &running_access_list);

    // test
    mod_lighttpd_update_accesses("update-accesses", &new_accesses, &ret);

    assert_int_equal(amxc_var_compare(&local_gui_access_expected, GET_ARG(&running_access_list, "LocalGUI"), &result), 0);
    assert_int_equal(result, 0);

    amxc_var_delete(&ipv4);
    amxc_var_delete(&ipv6_list);
    amxc_var_clean(&new_accesses);
    amxc_var_clean(&running_access_list);
    amxc_var_clean(&local_gui_access_expected);
    amxc_var_clean(&ret);
}

void test_mod_lighttpd_rpc_update_accesses_with_lla_address_only(UNUSED void** state) {
    int result = 0;
    amxc_var_t ret;
    amxc_var_t running_access_list;
    amxc_var_t new_accesses;
    amxc_var_t local_gui_access_expected;

    amxc_var_init(&local_gui_access_expected);
    amxc_var_init(&ret);
    amxc_var_init(&new_accesses);
    amxc_var_init(&running_access_list);

    // create access given by netmodel
    amxc_var_set_type(&new_accesses, AMXC_VAR_ID_LIST);

    amxc_var_t* local_gui_access = local_gui_access = amxc_var_add_new(&new_accesses);
    amxc_var_set_type(local_gui_access, AMXC_VAR_ID_HTABLE);

    amxc_var_t* ipv4 = NULL;
    amxc_var_new(&ipv4);
    fill_access_ipv4_entry(ipv4, "192.168.1.1");

    amxc_var_t* ipv6_list = NULL;
    amxc_var_new(&ipv6_list);
    amxc_var_set_type(ipv6_list, AMXC_VAR_ID_LIST);
    amxc_var_t* ipv6_lla = amxc_var_add_new(ipv6_list);
    fill_access_ipv6_entry(ipv6_lla, "fe80::2e93:fbff:fe15:72a1", "@lla");

    fill_access(local_gui_access, "LocalGUI", ipv4, ipv6_list);

    // running access
    amxc_var_set_type(&running_access_list, AMXC_VAR_ID_HTABLE);
    amxc_var_t* local_gui_access_running = amxc_var_add_new_key(&running_access_list, "LocalGUI");
    amxc_var_set_type(local_gui_access_running, AMXC_VAR_ID_HTABLE);

    amxc_var_t* ipv42 = amxc_var_add_new_key(local_gui_access_running, "IPv4");
    fill_access_ipv4_entry(ipv42, "192.168.1.1");
    amxc_var_add_key(amxc_htable_t, local_gui_access_running, "IPv4", amxc_var_get_const_amxc_htable_t(ipv42));

    amxc_var_t* ipv62 = amxc_var_add_new_key(local_gui_access_running, "IPv6");
    fill_access_ipv6_entry(ipv62, "2001:470:cb55:39f0::1", "@gua");
    amxc_var_add_key(amxc_htable_t, local_gui_access_running, "IPv6", amxc_var_get_const_amxc_htable_t(ipv62));

    // result expected
    amxc_var_copy(&local_gui_access_expected, local_gui_access);
    amxc_var_t* to_remove = GET_ARG(&local_gui_access_expected, "IPv6");
    amxc_var_take_it(to_remove);
    amxc_var_delete(&to_remove);

    will_return(__wrap_mod_lighttpd_get_runned_accesses, &running_access_list);
    will_return(__wrap_mod_lighttpd_generate_configs, 0);
    will_return(__wrap_mod_lighttpd_command_restart_server, 0);

    // test
    mod_lighttpd_update_accesses("update-accesses", &new_accesses, &ret);

    assert_int_equal(amxc_var_compare(&local_gui_access_expected, GET_ARG(&running_access_list, "LocalGUI"), &result), 0);
    assert_int_equal(result, 0);

    amxc_var_delete(&ipv4);
    amxc_var_delete(&ipv6_list);
    amxc_var_clean(&new_accesses);
    amxc_var_clean(&running_access_list);
    amxc_var_clean(&local_gui_access_expected);
    amxc_var_clean(&ret);
}