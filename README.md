# mod_httpaccess_lighttpd

[[_TOC_]]

# Introduction

This module is used by `tr181-httpaccess`. When used it will start a lighttpd server with the configuration passed by `tr181-httpaccess`.

# How to use it

1. Install `lighttpd` package on your target.
1. Install `tr181-httpaccess` plugin on your target.
1. Install this module (`mod_httpaccess_lighttpd`) on your target.
1. In the data-model, add `mod-httpaccess-lighttpd` in the list of supported controllers: `UserInterface.HTTPAccessSupportedControllers`.
1. In the data-model, change the value of `UserInterface.Controller` to `mod-httpaccess-lighttpd`.

# API

| Functions | Explanation |
| :-------- | :---------- |
| init | Initialize the module with configuration variables |
| update-accesses | Pass a list of access' configurations describing the different accesses |
| delete-access | Pass the alias of an access to remove it|

## init
### Input
| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| mod-lighttpd.conf-file | The path of the global configuration file | string |
| mod-lighttpd.mod-dir | The directory where the access' configuration files will be created | string |
| mod-lighttpd.conf-template | The path of a template file that will be used to create the global configuration file | string |
| mod-lighttpd.chown-dir | The path of a directory that needs its ownership changed so that lighttpd can access it in case of privilege deescalation (e.g. /webui) | string |
| mod-lighttpd.chown-owner | The username of the chown-dir new owner:group (e.g. lighttpd:lighttpd) | string |

### Output

| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| ------------- | ------------------ | ------- |

## update-accesses
### Input

The input is a list of amxc_variant here is a description of the item.

| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| 0.Alias | The access' alias | string |
| 0.Port | The access' port| uint32_t |
| 0.IPv4.Address | The access' ipv4 address | string |
| 0.IPv6.Address | The access' ipv6 address | string |
| 0.Role | The access' allowed role for authentication | string |
| 0.Order | The access' order of precedency | uint32_t |
| 0.Protocol | The access' protocol (HTTP/HTTPS) | string |
| 0.0.VHosts.DocumentRoot | The access' vhost's document root | string |
| 0.0.VHosts.Host | The access' vhost's host(url) to access it | string |
| 0.0.VHosts.Prefix | The access' vhost's prefix(url) to access it | string |
| 0.0.Proxies.IP | The access' proxy's ip address | string |
| 0.0.Proxies.Port | The access' proxy's port | uint32_t |
| 0.0.Proxies.Host | The access' proxy's host(url) to access it | string |
| 0.0.Proxies.Prefix | The access' proxy's prefix(url) to access it | string |

### Output
| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| 0.[Alias of the access] | Has the access started | boolean |

## delete-access
### Input
| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| Alias | The access' alias to delete | string |

### Output
| Variable | Explanation | Type |
| :------- | :---------- | :--: |
| ------------- | Has the deletion succeded | boolean |

# Configuration

To configure this module the init function needs to be called. The configuration variable are set in the `tr181-httpaccess` odl file.

```
//mod configurations default example
    mod-lighttpd = {
        conf-file = "/etc/lighttpd/mod/lighttpd.conf",
        mod-dir = "/etc/lighttpd/mod/conf/",
        conf-template = "/etc/lighttpd/mod/lighttpd.template",
    };
```
